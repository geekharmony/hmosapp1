// hsp
{
  "license": "",
  "devDependencies": {},
  "author": "geekapp",
  "name": "phone1",
  "description": "Please describe the basic information.",
  "main": "",
  "version": "1.0.0",
  "dependencies": {
    "@ohos/lottie": "^2.0.0",
    "@geekapp/librarycommon1": "file:../../feature/hsp/librarycommon1",
    "@ohos/libothergsyplayer": "file:../../feature/hsp/libothergsyplayer",
    "@ohos/libindex1": "file:../../feature/hsp/libindex1",
    "@geekapp/libraryindex4": "file:../../feature/hsp/libraryindex4",
    "@geekapp/libraryindex1": "file:../../feature/hsp/libraryindex1",
    "@geekapp/libraryindex2": "file:../../feature/hsp/libraryindex2"
  }
}

{
  "module": {
    "name": "libindex1",
    "type": "shared",
    "description": "$string:shared_desc",
    "deviceTypes": [
      "phone",
      "tablet"
    ],
    "deliveryWithInstall": true,
    "pages": "$profile:main_pages",
    "requestPermissions": [
      {
        "name": "ohos.permission.INTERNET"
      },
      {
        "name": "ohos.permission.GET_NETWORK_INFO"
      },
      {
        "name": "ohos.permission.GET_WIFI_INFO"
      }
    ],
    "metadata": [
      {
        "name": "ArkPartialUpdate",
        "value": "true"
      }
    ]
  }
}

// har
{
  "license": "",
  "devDependencies": {},
  "author": "geekapp",
  "name": "phone1",
  "description": "Please describe the basic information.",
  "main": "",
  "version": "1.0.0",
  "dependencies": {
    "@ohos/lottie": "^2.0.0",
    "@geekapp/librarycommon1": "file:../../feature/libs/librarycommon1.har",
    "@ohos/libothergsyplayer": "file:../../feature/libs/libothergsyplayer.har",
    "@ohos/libindex1": "file:../../feature/libs/libindex1.har"
  }
}



{
  "module": {
    "name": "library",
    "type": "har",
    "deviceTypes": [
      "default",
      "tablet",
      "2in1"
    ]
  }
}


// hsp har 获取raw
 let rawfileFd = await this.context.createModuleContext(this.tag1)
        .resourceManager
        .getRawFd(songItem.src)
        .catch((error: BusinessError) => {
          MusicLogger.error(`resourceManager error code ${error.code} message ${error.message}`);
        })































