/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import common from '@ohos.app.ability.common';
import preferences from '@ohos.data.preferences';
import router from '@ohos.router';
import { CustomDialogComponent } from '../common/CustomDialogComponent';
import { Lotties11 } from '../common/Lotties11';
import { advertising, identifier } from '@kit.AdsKit';
import { abilityAccessCtrl } from '@kit.AbilityKit';
import { BusinessError } from '@kit.BasicServicesKit';
import { AnimationItem } from '@ohos/lottie';
import { AdvertisingPage1 } from './AdvertisingPage1';
import { RouterUrlConstants } from '@ohos/libcommon1';
import { AdType, GlobalContext, Logger, WindowUtil  } from '@geekapp/libgeekapp';
import HiAdLog from '../HiAdLog';

/**
 * The LauncherPage is the entry point of the application and shows how to develop the LauncherPage.
 * Stay on the LauncherPage for a few seconds to jump to the AdvertisingPage.
 * Developers can replace the background image.
 */
@Entry
@Component
struct LauncherPage {
  @State message: string = 'Hello World'
  public renderingSettings: RenderingContextSettings = new RenderingContextSettings(true);
  public renderingContext: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.renderingSettings);
  public animateItem: AnimationItem | null = null;
  public animateName: string = 'data';
  public loop: number = 0;
  public autoplay: boolean = true;
  public lujing: string = './data.json';
  // 广告bufen
  private context1: common.UIAbilityContext = getContext(this) as common.UIAbilityContext;
  private oaid: string = '';
  // 广告展示参数
  private adDisplayOptions: advertising.AdDisplayOptions = {
    // 是否静音，默认不静音
    mute: false,

  }
  // 广告配置
  private adOptions: advertising.AdOptions = {}
  // 开屏视频广告请求参数
  private splashVideoAdReqParams: advertising.AdRequestParams = {
    adId: 'testd7c5cewoj6',
    adType: AdType.SPLASH_AD,
    adCount: 1,
  }
  // 开屏图片广告请求参数
  private splashImageAdReqParams: advertising.AdRequestParams = {
    adId: 'testq6zq98hecj',
    adType: AdType.SPLASH_AD,
    adCount: 1,
  }
  @Provide('LauncherPage') pageInfos: NavPathStack = new NavPathStack()

  @Builder
  PageMap(name: string) {
    AdvertisingPage1()
  }

  private requestAd(adReqParams: advertising.AdRequestParams, adOptions: advertising.AdOptions): void {
    // 给AdRequestParams设置oaid参数
    adReqParams.oaid = this.oaid;
    // 广告请求回调监听
    const adLoaderListener: advertising.AdLoadListener = {
      // 广告请求失败回调
      onAdLoadFailure: (errorCode: number, errorMsg: string) => {
        HiAdLog.i("TAG", `request ad errorCode is: ${errorCode}, errorMsg is: ${errorMsg}`);
        // Prompt.showToast({
        //   message: 'request ad failed, code is: ' + errorCode + 'message is: ' + errorMsg,
        //   duration: 1000
        // });
        // api 9
        // router.pushUrl({
        //   url: '@bundle:com.example.harmonyapp1/librarycommon1/ets/common/welcome/AdvertisingPage'
        // }).catch((error: Error) => {
        //   logger.error('LauncherPage', 'LauncherPage pushUrl error ' + JSON.stringify(error));
        // });
        // api 12
        this.pageInfos.pushPathByName('AdvertisingPage1', null)
      },
      // 广告请求成功回调
      onAdLoadSuccess: (ads: Array<advertising.Advertisement>) => {
        HiAdLog.i("TAG", "request ad success!");
        // 保存请求到的广告内容用于展示
        HiAdLog.i("TAG", `ads[0].adType is : ${JSON.stringify(ads[0])}`);
        if (canIUse("SystemCapability.Advertising.Ads")) {
          if (ads[0].adType === AdType.SPLASH_AD) {
            // 调用开屏广告展示页面
            if (ads[0]?.isFullScreen === true) {
              routePage(RouterUrlConstants.jump_ads1, ads, this.adDisplayOptions);
            } else {
              routePage(RouterUrlConstants.jump_ads2, ads, this.adDisplayOptions);
            }
          } else {
            HiAdLog.w("TAG", 'error adType');
          }
        }
      }
    };
    // 创建AdLoader广告对象
    const load: advertising.AdLoader = new advertising.AdLoader(this.context1);
    // 调用广告请求接口
    HiAdLog.i("TAG", 'request ad!');
    load.loadAd(adReqParams, adOptions, adLoaderListener);
  }

  private requestOAIDTrackingConsentPermissions(context: common.Context): void {
    // 进入页面时触发动态授权弹框，向用户请求授权广告跟踪权限
    const atManager: abilityAccessCtrl.AtManager = abilityAccessCtrl.createAtManager();
    try {
      atManager.requestPermissionsFromUser(context, ["ohos.permission.APP_TRACKING_CONSENT"]).then((data) => {
        if (data.authResults[0] == 0) {
          HiAdLog.i("TAG", 'request permission success');
          identifier.getOAID().then((data: string) => {
            this.oaid = data;
            HiAdLog.i("TAG", `getAdsIdentifierInfo by promise success: ${this.oaid}`);
          }).catch((error: BusinessError) => {
            HiAdLog.e("TAG", `getAdsIdentifierInfo failed, message: ${error.message}`);
          })
        } else {
          HiAdLog.i("TAG", 'user rejected');
        }
      }).catch((err: BusinessError) => {
        HiAdLog.e("TAG", `request permission failed, error: ${err.code} ${err.message}`);
      })
    } catch (err) {
      HiAdLog.e("TAG", `catch err->${err.code}, ${err.message}`);
    }
  }

  // 广告bufen


  aboutToAppear() {
    // setTimeout(() => {
    //   router.replaceUrl({ url: "pages/MainPage" })
    // }, 2000)
    try {
      // 使用Promise回调方式获取OAID
      this.requestOAIDTrackingConsentPermissions(this.context1);
    } catch (error) {
      HiAdLog.e("TAG", `catch err->${JSON.stringify(error)}`);
    }
    HiAdLog.i("TAG", 'aboutToAppear');
  }

  private context?: common.UIAbilityContext;
  private timerId: number = 0;
  private isJumpToAdvertising: boolean = false;
  dialogController: CustomDialogController = new CustomDialogController({
    builder: CustomDialogComponent(
      {
        cancel: () => {
          this.onCancel();
        },
        confirm: () => {
          this.onConfirm();
        }
      }),
    alignment: DialogAlignment.Bottom,
    offset: { dx: 0, dy: -24 },
    customStyle: true,
    autoCancel: false
  });

  onCancel() {
    // Exit the application.
    this.context?.terminateSelf();
  }

  onConfirm() {
    // Save privacy agreement status.
    this.saveIsPrivacy();
    this.jumpToAdvertisingPage();
  }

  onPageHide() {
    //
    WindowUtil.updateStatusBarColor(getContext(this), false);
    // window.getLastWindow(getContext(this), (err, w) => {
    //   w.setWindowLayoutFullScreen(false)
    //   w.setWindowSystemBarEnable(['status'])
    //
    // })
    //
    if (this.isJumpToAdvertising) {
      router.clear();
    }
    // globalThis.isJumpPrivacy = true;
    GlobalContext.getContext().setObject('isJumpPrivacy', true);
    clearTimeout(this.timerId);
  }

  onPageShow() {
    //
    WindowUtil.updateStatusBarColor(getContext(this), true);
    // window.getLastWindow(getContext(this), (err, w) => {
    //   w.setWindowLayoutFullScreen(true)
    //   w.setWindowSystemBarEnable(['navigation'])
    // })
    //
    this.context = getContext(this) as common.UIAbilityContext;
    // Get the operation class for saving data.
    this.getDataPreferences(this).then((preferences: preferences.Preferences) => {
      preferences.get('isPrivacy', true).then((value: preferences.ValueType) => {
        Logger.info('LauncherPage', 'onPageShow value: ' + value);
        if (value) {
          // let isJumpPrivacy: boolean = globalThis.isJumpPrivacy ?? false;
          let isJumpPrivacy: boolean = (GlobalContext.getContext().getObject('isJumpPrivacy') as boolean) ?? false;
          if (!isJumpPrivacy) {
            this.dialogController.open();
          }
        } else {
          this.jumpToAdvertisingPage();
        }
      });
    });
  }

  /**
   * Jump to advertising page.
   */
  jumpToAdvertisingPage() {
    this.timerId = setTimeout(() => {
      this.isJumpToAdvertising = true;
      //api9 方法1
      // router.pushUrl({
      //   url: '@bundle:com.example.harmonyapp1/librarycommon1/ets/common/welcome/AdvertisingPage'
      //   // url: '@bundle:com.example.harmonyapp1/librarycommon1/ets/common/ads/SplashFullScreenAdPage'
      //   // url: '@bundle:com.example.harmonyapp1/librarycommon1/ets/pages/MMkvIndex2'
      // }).catch((error: Error) => {
      //   logger.error('LauncherPage', 'LauncherPage pushUrl error ' + JSON.stringify(error));
      // });
      // 方法2
      // 跳转到开屏视频广告展示页面
      this.requestAd(this.splashVideoAdReqParams, this.adOptions);
    }, 1500);
  }

  saveIsPrivacy() {
    let preferences: Promise<preferences.Preferences> = this.getDataPreferences(this);
    preferences.then((result: preferences.Preferences) => {
      let privacyPut = result.put('isPrivacy', false);
      result.flush();
      privacyPut.then(() => {
        Logger.info('LauncherPage', 'Put the value of startup Successfully.');
      }).catch((err: Error) => {
        Logger.error('LauncherPage', 'Put the value of startup Failed, err: ' + err);
      });
    }).catch((err: Error) => {
      Logger.error('LauncherPage', 'Get the preferences Failed, err: ' + err);
    });
  }

  /**
   * Get data preferences action.
   */
  getDataPreferences(common: Object) {
    return preferences.getPreferences(getContext(common), 'myStore');
  }

  build() {
    // Stack() {
    //   Image(ResUtils.get_icon2())
    //     .width('100%')
    //     .height('100%')
    //   Column() {
    //     // Image(ResUtils.get_icon2())
    //     //   .width(ResUtils.get_size_vp_119())
    //     //   .height(ResUtils.get_size_vp_119())
    //     //   .margin({ top: '16.2%' })
    //     // Text(ResUtils.get_healthy_life_text())
    //     //   .width(ResUtils.get_size_vp_105())
    //     //   .height(ResUtils.get_size_vp_35())
    //     //   .healthyLifeTextStyle(FontWeight.Bold,
    //     //     0.1,
    //     //     ResUtils.get_size_fp_26(),
    //     //     ResUtils.get_color182431())
    //     //   .margin({ top: '0.5%' })
    //     // Text(ResUtils.get_healthy_life_introduce())
    //     //   .height('2.7%')
    //     //   .healthyLifeTextStyle(FontWeight.Normal,
    //     //     3.4,
    //     //     ResUtils.get_color182431(),
    //     //     ResUtils.get_color182431())
    //     //   .opacity(ResUtils.get_color182431())
    //     //   .margin({ top: '1.3%' })
    //     Lotties2()
    //     Text('鸿蒙星河计划').fontSize(36).fontColor(ResManager.getDefaultColor())
    //     // Blank()
    //   }.flexGrow(1).justifyContent(FlexAlign.Center)
    //
    //   Text(ResUtils.get_healthy_life_introduce()).fontSize(10).margin({ bottom: 40 }).fontColor(0xAAABAB)
    // }
    Navigation(this.pageInfos) {
      Stack() {
        Image($r('app.media.ic_launcher_background'))
          .width('100%')
          .height('100%')
        Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
          Column() {
            Lotties11()
            Text('鸿蒙星河计划').fontSize(36).fontColor('#FE5756')
          }.flexGrow(1).justifyContent(FlexAlign.Center)

          Text('harmonyos技术支持').fontSize(10).margin({ bottom: 40 }).fontColor(0xAAABAB)
        }
      }
    }
    .title('LauncherPage')
    .navBarWidth('50%')
    .navDestination(this.PageMap)
    .mode(NavigationMode.Stack)
    .hideTitleBar(true)

  }
}

async function routePage(pageUri: string, ads: Array<advertising.Advertisement | null>, displayOptions: advertising.AdDisplayOptions) {
  let options: router.RouterOptions = {
    url: pageUri,
    params: {
      ads: ads,
      displayOptions: displayOptions,
    }
  }
  try {
    HiAdLog.i("TAG", `routePage  + ${pageUri}`);
    router.pushUrl(options);
  } catch (error) {
    HiAdLog.e("TAG", `routePage fail callback, code: ${error.code}, msg: ${error.message}`);
  }
}

// Healthy living text common styles.
@Extend(Text)
function healthyLifeTextStyle(fontWeight: number,
                              textAttribute: number, fontSize: Resource, fontColor: Resource) {
  .fontWeight(fontWeight)
  .letterSpacing(textAttribute)
  .fontSize(fontSize)
  .fontColor(fontColor)
}