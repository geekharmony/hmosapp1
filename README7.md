# 应用间跳转

# 介绍
跳转短信、浏览器、设置、相机、拨号、应用市场等系统应用和已知bundlename、abilityname的三方应用。

跳转相机拍照后返回照片展示。

跳转三方应用，模拟简易支付。

跳转web页，拉起相机和三方应用。

# 效果预览
![应用间跳转效果预览](screenshots/PullOtherApp.gif)

# 工程目录
工程的目录结构
```
├──entry                                          // 入口模块
│  ├──build-profile.json5                         // 编译配置文件，其中arkOptions需配置动态import依赖的包名
│  ├──oh-package.json5                            // 依赖配置，需依赖全部子业务模块和RouterModule模块
│  ├──src/main/ets
│  │  ├──components
│  │  │  ├──JumpAppStore.ets                      // 跳转应用市场逻辑页
│  │  │  ├──JumpCamera.ets                        // 跳转相机逻辑页
│  │  │  └──JumpWeb.ets                           // 跳转web逻辑页
│  │  ├──entryability
│  │  │  └──EntryAbility.ets
│  │  ├──pages
│  │  │   └──Index.ets                            // 首页
│  │  ├──utils                                    // 工具类目录
│  │  │  └──SimpleAlertDialog.ets                 // 弹窗工具类
│  └──src/main/resources                          // 资源目录
│     └──rawfile
│        └──index.html                            // web页

```

# 具体实现
基本分两大类方式：

1、通过上下文的startAbility能力拉起应用，并获取返回数据。

- 例如模拟拉起三方应用支付的能力，需要三方应用中配合开发（或者说按特定规则开发）;

- 本Demo中要拉起的支付应用bundleName: 'com.example.test1',主页Index.ets和entryAbility.ets文件存在entry模块下test1目录中，新建一个com.example.test1工程，将这两个文件替换进去即可。

2、通过特定系统API跳转到系统应用中。

# 相关权限
不涉及。
# 约束与限制
1. 本示例仅支持标准系统上运行，支持设备：华为手机。
2. 手机ROM版本高于等于：NOH-AN00 204.1.0.72(SP2DEVC00E72R1P1)
3. IDE高于等于：DevEco Studio 4.1.3.700
4. SDK高于等于：HarmoneyOS NEXT Developer Preview2 B.0.72（API 11）
