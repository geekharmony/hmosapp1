# hint-dialog

#### 介绍

每个项目都需要标配的《提示对话框》。特点：使用简单、支持各种样式定制、支持Text和Web显示。

![gif显示](https://gitee.com/qincji/hint-dialog/raw/master/demo.gif)

#### 安装教程

```
ohpm install @qincji/dialog
```

#### 使用说明

1. 导入相关文件

```
import { HintDialog, xxx } from '@qincji/dialog';
```

2. 在@Entry等页面的build节点中添加初始化。

```
  build() {
    Row() {
      HintDialog() //这是空布局
      ...
    }
  }
```

> 主：通常只需要在MainPage.ets 中初始化一次，但必须要存在。router当跳转到其他页面也不用再初始化。

3. 打开

```
HintDialog.open(hintParm: HintParm)
```

案例1：

```
HintDialog.open({
    content: '这是内容，使用默认标题。',
    showNo: false,
    onOk: () => {
    }
})
```

案例2：

```
HintDialog.open({
    title: '单个按钮',
    content: '一个按钮，更改标题。',
    showNo: false,
    okMsg: '好，我知道了',
    onOk: () => {
    }
  })
```

案例3：

```
HintDialog.open({
  title: 'Web对话框',
  isWebType: true,
  content: 'https://gitee.com/qincji/hint-dialog',
  onNo: () => {
  }
})
```

##### HintParm 参数说明

| 字段            | 类型                 | 介绍                                                                   |
|---------------|--------------------|----------------------------------------------------------------------|
| content       | string             | 显示的内容，根据isWebType来显示web还是text。如果为 ''或undefined则去HintStyle里面的。        |
| title         | string?            | 标题，默认："温馨提示"                                                         |
| noMsg         | string?            | 取消按钮的信息，默认："取消"                                                      |
| okMsg         | string?            | 确定按钮的信息，默认："确定"                                                      |
| showNo        | boolean?           | 是否需要显示取消按钮，默认：true                                                   |
| isWebType     | boolean?           | 是否是web类型，默认：false                                                    |
| controller    | WebviewController? | web控制器，isWebType为true时有效。                                            |
| outsideCancel | boolean?           | 点击外面区域是否可以取消，默认 true。因为系统不支持拦截 @CustomDialog 的返回键，因此只能当做用户点击了"取消"来看待 |
| onNo          | () => void?        | 点击"取消"的回调                                                            |
| onOk          | () => void?        | 点击"确定"的回调                                                            |
| alignment     | DialogAlignment?   | 位置，默认居中。                                                             |

> 注：string? ：不是可以不传。

4. 关闭（非必须）

```
HintDialog.close()
```

5. 全局自定义样式（非必须）

```
HintDialog.initDefaultStyle(hintStyle: HintStyle)
```

案例：

```
const titleAttr: AttributeModifier<TextAttribute> = {
  applyNormalAttribute(instance: TextAttribute): void {
    instance.fontColor('#ff1145e5').fontWeight(FontWeight.Bolder).fontSize(25)
  }
};
const outBoxAttr: AttributeModifier<ColumnAttribute> = {
  applyNormalAttribute(instance: ColumnAttribute): void {
    instance.backgroundColor('#ffc8f1b3').borderRadius(5).margin(50)
  }
};
HintDialog.initDefaultStyle({
  titleParm: new TitleParm('默认提示', titleAttr),
  outBoxAttr: outBoxAttr
})
```

##### HintStyle 参数说明

| 字段            | 类型                                  | 介绍                                                                   |
|---------------|-------------------------------------|----------------------------------------------------------------------|
| isWebType     | boolean？                            | 是否是web类型，默认：false                                                    |
| outsideCancel | boolean？                            | 点击外面区域是否可以取消，默认 true。因为系统不支持拦截 @CustomDialog 的返回键，因此只能当做用户点击了"取消"来看待 |
| alignment     | DialogAlignment?                    | 位置，默认居中。                                                             |
| titleParm     | TitleParm?                          | 设置标题内容和样式，默认标题"温馨提示"                                                 |
| textParm      | TextParm?                           | text加载内容的样式                                                          |
| webParm       | WebParm?                            | web加载内容的样式                                                           |
| btnParm       | BtnParm?                            | 底部按钮的样式和内容                                                           |
| outBoxAttr    | AttributeModifier<ColumnAttribute>? | 最外层UI的样式，通过该属性设置对话框的背景颜色、圆角、距离屏幕的距离等。                                |

