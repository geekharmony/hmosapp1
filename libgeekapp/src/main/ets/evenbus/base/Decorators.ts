import { EventBus } from './EventBus'

export function Subscriber(TypeName: string, sticky: boolean = false) {
  return (target: any, _: string, propertyDescriptor: PropertyDescriptor) => {
    if (target.rerender) {
      if (target.aboutToDisappear) {
        let oldFunction = target.aboutToDisappear
        function disappear(){
          EventBus.getDefault().off(TypeName, propertyDescriptor.value)
          oldFunction.call(disappear.prototype.caller)
        }
        target.aboutToDisappear = disappear
      } else {
        target.aboutToDisappear = () => {
          EventBus.getDefault().off(TypeName, propertyDescriptor.value)
        }
      }

      if (target.aboutToAppear) {
        let oldFunction = target.aboutToAppear
        function appear(){
          EventBus.getDefault().on(TypeName, propertyDescriptor.value, sticky, this)
          oldFunction.call(this)
        }
        target.aboutToAppear = appear
      } else {
        function appear(){
          EventBus.getDefault().on(TypeName, propertyDescriptor.value, sticky, this)
        }
        target.aboutToAppear = appear
      }
    }
  }
}