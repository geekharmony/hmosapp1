import { Dispatch } from './Dispatch'

export class EventBus {
  private dispatch: Dispatch = new Dispatch()

  private constructor() {
  }

  public static getDefault(): EventBus {
    if (!globalThis.EventBus) {
      globalThis.EventBus = {}
    }
    if (!globalThis.EventBus.default) {
      let that = new EventBus()
      globalThis.EventBus.default = that
    }
    return globalThis.EventBus.default
  }

  post(TypeNam: string, ...args: any[]) {
    this.dispatch.emit(TypeNam, args)
  }

  on(TypeName: string, callback: Function, sticky: boolean = false, callThis?: any) {
    this.dispatch.on(TypeName, callback, callThis, sticky)
  }

  off(TypeName: string, callback: Function) {
    this.dispatch.off(TypeName, callback)
  }

  once(TypeName: string, callback: Function, callThis?: any) {
    this.dispatch.once(TypeName, callback, callThis)
  }
}