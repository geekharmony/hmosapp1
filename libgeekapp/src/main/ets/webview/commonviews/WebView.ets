import { webview } from '@kit.ArkWeb';
import { FailureLayout } from '../../uicomponents/components/emptyview/FailureLayout';
import { LoadingLayout } from '../../uicomponents/components/emptyview/LoadingLayout';
import { CommonConstants } from '../../utils/model/CommonConstants';
import { ColumnEnum, OffsetEnum, SpanEnum, WebpageStatus } from '../../utils/model/CommonEnums';
import Logger from '../utils/Logger';

const TAG = '[WebView]';

@Component
export struct WebView {
  handlePageEnd?: () => void;
  shouldShowExtBtn?: () => void;
  hiddenScrollBar?: boolean = false;
  url: ResourceStr = '';
  isLarge?: boolean = true;
  changeFontSize: number = 16; // 全局字体大小
  @Link webViewController: webview.WebviewController;
  @State webStatus: WebpageStatus = WebpageStatus.INIT;

  build() {
    GridRow({
      columns: this.isLarge ? { sm: ColumnEnum.SM, md: ColumnEnum.MD, lg: ColumnEnum.LG } : 1,
      gutter: {
        x: {
          sm: $r('app.float.xxl_padding_margin'),
          md: $r('app.float.md_padding_margin'),
          lg: $r('app.float.md_padding_margin')
        }
      },
      breakpoints: { reference: BreakpointsReference.ComponentSize }
    }) {
      GridCol({
        span: this.isLarge ? { sm: SpanEnum.SM, md: SpanEnum.MD, lg: SpanEnum.LG } : 1,
        offset: this.isLarge ? { sm: OffsetEnum.SM, md: OffsetEnum.MD, lg: OffsetEnum.LG } : 0
      }) {
        Stack() {
          Web({ controller: this.webViewController, src: this.url })
            .darkMode(WebDarkMode.Auto)
            .domStorageAccess(true)
            .defaultFontSize(this.changeFontSize)
            .zoomAccess(true)
            .fileAccess(true)
            .mixedMode(MixedMode.All)
            .cacheMode(CacheMode.None)
            .verticalScrollBarAccess(!this.hiddenScrollBar)
            .javaScriptAccess(true)
            .width(CommonConstants.FULL_PERCENT)
            .onProgressChange((event) => {
              Logger.info(TAG, 'newProgress:' + event?.newProgress);
              if (event?.newProgress === 100) {
                this.shouldShowExtBtn && this.shouldShowExtBtn();
              }
              Logger.info(TAG, 'onProgressChange weburl: ' + this.webViewController.getUrl());
            })
            .onPageBegin(() => {
              Logger.info(TAG, ' onPageBegin start loading');
              Logger.info(TAG, 'onPageBegin weburl: ' + this.webViewController.getUrl());
            })
            .onErrorReceive(() => {
              this.webStatus = WebpageStatus.FINISHED;
            })
            .onPageEnd((event) => {
              Logger.info(TAG, 'onPageEnd loading completed url: ' + event?.url);
              Logger.info(TAG, 'onPageEnd weburl: ' + this.webViewController.getUrl());
              if (this.webStatus != WebpageStatus.ERROR) {
                this.webStatus = WebpageStatus.FINISHED;
                this.handlePageEnd && this.handlePageEnd();
              }
            })

          if (this.webStatus === WebpageStatus.INIT) {
            Column() {
              LoadingLayout()
            }
            .backgroundColor(Color.White)
          }

          if (this.webStatus === WebpageStatus.ERROR) {
            Column() {
              FailureLayout({
                handleReload: () => {
                  this.webViewController.refresh();
                  this.webStatus = WebpageStatus.INIT;
                }
              })
            }
            .justifyContent(FlexAlign.Center)
            .alignItems(HorizontalAlign.Center)
            .backgroundColor(Color.White)
            .width(CommonConstants.FULL_PERCENT)
            .height(CommonConstants.FULL_PERCENT)
          }
        }
      }
    }
    .layoutWeight(1)
  }
}