import { CommonConstants } from '../../utils/model/CommonConstants';
import { BreakpointType, BreakpointTypeEnum } from '../../utils/BreakpointSystem';
import { WindowUtil } from '../../utils/WindowUtil';
import { TabBarData, TabBarType, TabsInfo } from '../model/TabBarModel';

@Component
export struct CustomTabBar {
  @Link currentIndex: TabBarType;
  @StorageLink('currentBreakpoint1') currentBreakpoint: BreakpointTypeEnum = BreakpointTypeEnum.SM;
  @StorageProp('naviIndicatorHeight') naviIndicatorHeight: number = 0;

  onChange(index: TabBarType): void {
    this.currentIndex = index;
    if (this.currentIndex === TabBarType.index5) {
      WindowUtil.updateStatusBarColor(getContext(this), true);
    } else {
      WindowUtil.updateStatusBarColor(getContext(this), false);
    }
  }

  build() {
    Flex({
      direction: this.currentBreakpoint === BreakpointTypeEnum.LG ? FlexDirection.Column : FlexDirection.Row,
      alignItems: ItemAlign.Center,
      justifyContent: FlexAlign.SpaceAround
    }) {
      ForEach(TabsInfo, (item: TabBarData) => {
        TabItem({
          index: item.id,
          selectedIndex: this.currentIndex,
          onChange: (index: number) => this.onChange(index)
        })
      }, (item: TabBarData) => item.id.toString())
    }
    .backgroundColor(this.currentBreakpoint === BreakpointTypeEnum.LG && this.currentIndex === TabBarType.index5 ?
      '#33FFFFFF' : '#F1F3F5')
    .backgroundBlurStyle(this.currentBreakpoint === BreakpointTypeEnum.LG && this.currentIndex === TabBarType.index5 ?
    BlurStyle.BACKGROUND_THIN : BlurStyle.NONE)
    .border({
      width: this.currentBreakpoint === BreakpointTypeEnum.LG ? { right: $r('app.float.tab_border_width') } : {
        top: $r('app.float.tab_border_width')
      },
      color: '#0D182431'
    })
    .padding(this.currentBreakpoint === BreakpointTypeEnum.LG ? {
      top: CommonConstants.TAB_PERCENT,
      bottom: CommonConstants.TAB_PERCENT
    } : { bottom: this.naviIndicatorHeight })
    .clip(false)
    .height(new BreakpointType<Length>({
      sm: CommonConstants.TAB_HEIGHT + (this.naviIndicatorHeight || 0),
      md: CommonConstants.TAB_HEIGHT + (this.naviIndicatorHeight || 0),
      lg: CommonConstants.FULL_PERCENT
    }).getValue(this.currentBreakpoint))
    .width(new BreakpointType<Length>({
      sm: CommonConstants.FULL_PERCENT,
      md: CommonConstants.FULL_PERCENT,
      lg: $r('app.float.tool_bar_width')
    }).getValue(this.currentBreakpoint))
  }
}

@Component
struct TabItem {
  @Prop index: number
  @Prop selectedIndex: number;
  onChange: (index: number) => void = () => {
  };
  @StorageLink('currentBreakpoint1') currentBreakpoint: BreakpointTypeEnum = BreakpointTypeEnum.MD;

  build() {
    Column() {
      Image(this.selectedIndex === this.index ? TabsInfo[this.index].activeIcon : TabsInfo[this.index].defaultIcon)
        .size(this.index === TabBarType.index3 ?
          { width: $r('app.float.tab_big_img_width'), height: $r('app.float.tab_big_img_height') } :
          { width: $r('app.float.tab_img_size'), height: $r('app.float.tab_img_size') })
        .margin({ top: this.index === TabBarType.index3 ? $r('app.float.tab_margin') : 0 })
      Text(TabsInfo[this.index].title)
        .fontSize($r('app.float.tab_font_size'))
        .margin({ top: $r('app.float.tab_title_margin') })
        .fontWeight(CommonConstants.TAB_FONT_WEIGHT)
        .fontColor(this.index === this.selectedIndex ?
          (this.index == TabBarType.index3 ? '#00CCD7' : $r('app.color.theme_blue_color')) : '#99000000')
    }
    .clip(false)
    .padding(this.currentBreakpoint === BreakpointTypeEnum.LG ?
      { top: $r('app.float.sm_padding_margin'), bottom: $r('app.float.sm_padding_margin') } :
      { left: $r('app.float.md_padding_margin'), right: $r('app.float.md_padding_margin') })
    .size(this.currentBreakpoint === BreakpointTypeEnum.LG ?
      { width: CommonConstants.FULL_PERCENT } :
      { height: CommonConstants.FULL_PERCENT })
    .justifyContent(FlexAlign.Center)
    .onClick(() => this.onChange(this.index))
  }
}