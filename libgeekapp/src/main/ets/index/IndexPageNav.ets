import { CommonSizeConstants } from '../fontsize/constants/CommonSizeConstants';
import { RouterNameConstants } from '../routermodule/router1/constants/RouterConstants';
import { RouterModule } from '../routermodule/router1/utils/RouterModule';
import { BreakpointSystem } from '../utils/BreakpointSystem';
import { common } from '@kit.AbilityKit';
import { MainConstants } from './constants/MainConstants';
import { AppUtils } from './constants/AppUtils';
import { TabBarType } from './model/TabBarModel';
import { SizePreferencesUtil } from '../fontsize/utils/SizePreferencesUtil';
import Logger from '../fontsize/utils/Logger';
import { WindowUtil } from '../utils/WindowUtil';
import { IndexPage } from './IndexPage';

const TAG = '[IndexPageNav]';
const context = (getContext(this) as common.UIAbilityContext);
const router_name1 = RouterNameConstants.ENTRY_HAP1;

@Entry({ routeName: 'IndexPageNav' })
@Component
export struct IndexPageNav {
  @State entryHapRouter: NavPathStack = new NavPathStack(); // 全局堆栈跳转
  @State breakpointSystem: BreakpointSystem = new BreakpointSystem();
  @State context: common.UIAbilityContext = getContext(this) as common.UIAbilityContext
  @State changeFontSize: number = CommonSizeConstants.SET_SIZE_NORMAL; // 全局字体大小
  @State fontSizeText: Resource = $r("app.string.set_size_normal");
  @State webViewController: WebviewController | null = null; // 全局WebView
  @LocalStorageLink('mainTabIndex') currentIndex: TabBarType = TabBarType.index1;

  @Builder
  routerMap(builderName: string, param: object) {
    RouterModule.getBuilder(builderName).builder(param);
  };

  build() {
    Navigation(this.entryHapRouter) {
      IndexPage({
        changeFontSize: this.changeFontSize,
        fontSizeText: this.fontSizeText,
        webViewController:$webViewController
      })
    }
    .title('首页')
    .hideTitleBar(true)
    .navDestination(this.routerMap)
    .mode(NavigationMode.Stack)
  }

  aboutToAppear() {
    // 堆栈
    AppUtils.getInstance().init_nav(this.entryHapRouter, router_name1);
    // 适配
    // this.breakpointSystem.register();
    // 横竖屏
    AppUtils.getInstance().init_display(MainConstants.foldExpanded);
    // app前后台状态
    WindowUtil.setMissionContinueActive(this.context, true);

  };

  aboutToDisappear() {
    //
    // this.breakpointSystem.unregister();
    //
    AppUtils.getInstance().del_display(MainConstants.foldExpanded);
    //
    WindowUtil.setMissionContinueActive(this.context, false);
  }

  onPageShow() {
    SizePreferencesUtil.getInstance().getChangeFontSize().then((value) => {
      this.changeFontSize = value;
      Logger.info("TAG", 'Get the value of changeFontSize: ' + this.changeFontSize);
    });
  }

  onDestinationBack(): boolean {
    if (this.webViewController?.accessBackward()) {
      this.webViewController.backward();
    } else {
      if (this.currentIndex === TabBarType.index5) {
        WindowUtil.updateStatusBarColor(getContext(this), true);
      }
      // this.appPathStack.pop();

    }
    return true;
  }

  onBackPress() {
    //
    SizePreferencesUtil.getInstance().getChangeFontSize().then((value) => {
      this.changeFontSize = value;
      Logger.info("TAG", 'Get the value of changeFontSize: ' + this.changeFontSize);
    });
    //
    if (this.webViewController?.accessBackward()) {
      this.webViewController.backward();
      return true;
    }
    return false;
  }
}
