import { advertising, identifier } from '@kit.AdsKit';
import { Prompt, router } from '@kit.ArkUI';
import { abilityAccessCtrl, common } from '@kit.AbilityKit';
import { BusinessError } from '@kit.BasicServicesKit';
import { ActionBar } from './widgets/action-bar';
import HiAdLog from './model/HiAdLog';
import { CustomButton } from './widgets/custom-button';
import { InterstitialAdStatusHandler } from './widgets/InterstitialAdStatusHandler';
import { AdType } from './model/AdType';
import { CommonUtil } from '../utils/CommonUtil';

const TAG = 'Ads Demo-LoadAd';

import('./SplashFullScreenAdPage')
import('./SplashHalfScreenAdPage')


@Component
export struct AdsIndex {
  private context: common.UIAbilityContext = getContext(this) as common.UIAbilityContext;
  private oaid: string = '';
  // 广告展示参数
  private adDisplayOptions: advertising.AdDisplayOptions = {
    // 是否静音，默认不静音
    mute: false,
  }
  // 广告配置
  private adOptions: advertising.AdOptions = {}
  // 插屏视频广告请求参数
  private interstitialVideoAdReqParams: advertising.AdRequestParams = {
    adId: 'testb4znbuh3n2',
    // adId: 'testd7c5cewoj6',
    adType: AdType.INTERSTITIAL_AD,
    adCount: 1,
  }
  // 插屏图片广告请求参数
  private interstitialImageAdReqParams: advertising.AdRequestParams = {
    adId: 'teste9ih9j0rc3',
    adType: AdType.INTERSTITIAL_AD,
    adCount: 1,
  }
  // 广告配置
  private adOptions2: advertising.AdOptions = {}
  // 开屏视频广告请求参数
  private splashVideoAdReqParams: advertising.AdRequestParams = {
    adId: 'testd7c5cewoj6',
    adType: AdType.SPLASH_AD,
    adCount: 1,
  }
  // 开屏图片广告请求参数
  private splashImageAdReqParams: advertising.AdRequestParams = {
    adId: 'testq6zq98hecj',
    adType: AdType.SPLASH_AD,
    adCount: 1,
  }

  aboutToAppear() {
    try {
      // 使用Promise回调方式获取OAID
      this.requestOAIDTrackingConsentPermissions(this.context);
    } catch (error) {
      HiAdLog.e(TAG, `catch err->${JSON.stringify(error)}`);
    }
    HiAdLog.i(TAG, 'aboutToAppear');
  }

  build() {
    Column() {
      ActionBar({ title: $r("app.string.interstitial_ads_demo_title") }).height('10%')
        .margin({ top: AppStorage.get<number>('statusBarHeight') })
      Column() {
        // 跳转到插屏视频广告展示页面
        CustomButton({
          mText: $r("app.string.request_interstitial_video_ad_btn"), mOnClick: () => {
            this.requestAd(this.interstitialVideoAdReqParams, this.adOptions);
          }
        });

        // 跳转到插屏图片广告展示页面
        CustomButton({
          mText: $r("app.string.request_interstitial_image_ad_btn"), mOnClick: () => {
            this.requestAd(this.interstitialImageAdReqParams, this.adOptions);
          }
        });

        // 跳转到插屏图片广告展示页面1
        CustomButton({
          mText: '跳转到插屏图片广告展示页面1', mOnClick: () => {
            this.requestAd2(this.splashVideoAdReqParams, this.adOptions);
          }
        });
        // 跳转到插屏图片广告展示页面2
        CustomButton({
          mText: '跳转到插屏图片广告展示页面2', mOnClick: () => {
            this.requestAd2(this.splashImageAdReqParams, this.adOptions);
          }
        });

      }.width('100%').height('80%').justifyContent(FlexAlign.Center)
    }
    .width('100%')
    .height('100%')
  }

  private requestAd(adReqParams: advertising.AdRequestParams, adOptions: advertising.AdOptions): void {
    adReqParams.oaid = this.oaid;
    // 广告请求回调监听
    const adLoaderListener: advertising.AdLoadListener = {
      // 广告请求失败回调
      onAdLoadFailure: (errorCode: number, errorMsg: string) => {
        HiAdLog.i(TAG, `request ad errorCode is: ${errorCode}, errorMsg is: ${errorMsg}`);
        Prompt.showToast({
          message: 'request ad failed, code is: ' + errorCode + 'message is: ' + errorMsg,
          duration: 1000
        });
      },
      // 广告请求成功回调
      onAdLoadSuccess: (ads: Array<advertising.Advertisement>) => {
        HiAdLog.i(TAG, "request ad success!");
        // 保存请求到的广告内容用于展示
        HiAdLog.i(TAG, `ads[0].adType is : ${JSON.stringify(ads[0])}`);
        // 注册插屏广告状态监听器，用于监听插屏广告播放状态
        new InterstitialAdStatusHandler().registerPPSReceiver();
        // 调用广告展示接口
        advertising.showAd(ads[0], this.adDisplayOptions, this.context);
      }
    };
    // 创建AdLoader广告对象
    const load: advertising.AdLoader = new advertising.AdLoader(this.context);
    // 调用广告请求接口
    HiAdLog.i(TAG, 'request ad!');
    load.loadAd(adReqParams, adOptions, adLoaderListener);
  }

  private requestAd2(adReqParams: advertising.AdRequestParams, adOptions: advertising.AdOptions): void {
    // 给AdRequestParams设置oaid参数
    adReqParams.oaid = this.oaid;
    // 广告请求回调监听
    const adLoaderListener: advertising.AdLoadListener = {
      // 广告请求失败回调
      onAdLoadFailure: (errorCode: number, errorMsg: string) => {
        HiAdLog.i("TAG", `request ad errorCode is: ${errorCode}, errorMsg is: ${errorMsg}`);
        // Prompt.showToast({
        //   message: 'request ad failed, code is: ' + errorCode + 'message is: ' + errorMsg,
        //   duration: 1000
        // });
        // api 9
        // router.pushUrl({
        //   url: '@bundle:com.example.harmonyapp1/librarycommon1/ets/common/welcome/AdvertisingPage'
        // }).catch((error: Error) => {
        //   logger.error('LauncherPage', 'LauncherPage pushUrl error ' + JSON.stringify(error));
        // });
        // api 12
        // this.pageInfos.pushPathByName('AdvertisingPage1', null)
      },
      // 广告请求成功回调
      onAdLoadSuccess: (ads: Array<advertising.Advertisement>) => {
        HiAdLog.i("TAG", "request ad success!");
        // 保存请求到的广告内容用于展示
        HiAdLog.i("TAG", `ads[0].adType is : ${JSON.stringify(ads[0])}`);
        if (canIUse("SystemCapability.Advertising.Ads")) {
          if (ads[0].adType === AdType.SPLASH_AD) {
            // 调用开屏广告展示页面
            if (ads[0]?.isFullScreen === true) {
              this.routePage('SplashFullScreenAdPage', CommonUtil.jump_ads1, ads, this.adDisplayOptions);
            } else {
              this.routePage('SplashHalfScreenAdPage', CommonUtil.jump_ads2, ads, this.adDisplayOptions);
            }
          } else {
            HiAdLog.w("TAG", 'error adType');
          }
        }
      }
    };
    // 创建AdLoader广告对象
    const load: advertising.AdLoader = new advertising.AdLoader(this.context);
    // 调用广告请求接口
    HiAdLog.i("TAG", 'request ad!');
    load.loadAd(adReqParams, adOptions, adLoaderListener);
  }

  private requestOAIDTrackingConsentPermissions(context: common.Context): void {
    // 进入页面时触发动态授权弹框，向用户请求授权广告跟踪权限
    const atManager: abilityAccessCtrl.AtManager = abilityAccessCtrl.createAtManager();
    try {
      atManager.requestPermissionsFromUser(context, ["ohos.permission.APP_TRACKING_CONSENT"]).then((data) => {
        if (data.authResults[0] == 0) {
          HiAdLog.i(TAG, 'request permission success');
          identifier.getOAID().then((data: string) => {
            this.oaid = data;
            HiAdLog.i(TAG, `getAdsIdentifierInfo by promise success: ${this.oaid}`);
          }).catch((error: BusinessError) => {
            HiAdLog.e(TAG, `getAdsIdentifierInfo failed, message: ${error.message}`);
          })
        } else {
          HiAdLog.i(TAG, 'user rejected');
        }
      }).catch((err: BusinessError) => {
        HiAdLog.e(TAG, `request permission failed, error: ${err.code} ${err.message}`);
      })
    } catch (err) {
      HiAdLog.e(TAG, `catch err->${err.code}, ${err.message}`);
    }
  }

  private routePage(name: string, pageUri: string, ads: Array<advertising.Advertisement | null>, displayOptions: advertising.AdDisplayOptions) {
    let options: router.RouterOptions = {
      url: pageUri,
      params: {
        ads: ads,
        displayOptions: displayOptions,
      }
    }
    try {
      HiAdLog.i("TAG", `routePage  + ${pageUri}`);
      // router.pushNamedRoute({ name: name, params: options });
      router.pushNamedRoute({
        name: name,
        params: {
          ads: ads,
          displayOptions: displayOptions,
        }
      });
    } catch (error) {
      HiAdLog.e("TAG", `routePage fail callback, code: ${error.code}, msg: ${error.message}`);
    }
  }
}
