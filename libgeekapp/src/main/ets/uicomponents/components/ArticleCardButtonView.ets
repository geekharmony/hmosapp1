import { CommonConstants } from '../../utils/model/CommonConstants';
import { CommonUtil } from '../../utils/CommonUtil';
import { LottieUtil } from '../../utils/LottieUtil';
import { Constants } from '../common/Constants';

@Component
export struct ArticleCardButtonView {
  @Prop isClicked: boolean = false;
  @Prop count: number = 0;
  @Prop textWidth: Resource = $r('app.float.default_icon_width');
  @Prop articleId: string = '';
  @Prop clickAnimationPath: string = '';
  @Prop cancelAnimationPath: string = '';
  @Prop type: string = '';
  @Prop normalImage: Resource = $r('app.media.ic_failure');
  @Prop onImage: Resource = $r('app.media.ic_failure');
  @State playingAnimation: boolean = false;
  onClicked?: () => void;
  private mainRenderingSettings: RenderingContextSettings = new RenderingContextSettings(true);
  private mainCanvasRenderingContext: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.mainRenderingSettings);

  build() {
    Row() {
      Stack() {
        if (!this.playingAnimation) {
          Image(this.isClicked ? this.onImage : this.normalImage)
            .margin({ right: $r('app.float.xs_padding_margin') })
            .size({ width: $r('app.float.default_icon_width'), height: $r('app.float.default_icon_width') })
        } else {
          Canvas(this.mainCanvasRenderingContext)
            .size({ width: $r('app.float.default_icon_width'), height: $r('app.float.default_icon_width') })
            .margin({ right: $r('app.float.xs_padding_margin') })
            .id('canvas' + this.articleId + this.type)
        }
      }

      Text(CommonUtil.transNumberOverOneThousand(this.count))
        .fontFamily(CommonConstants.HARMONY_HEI_TI_FONT_FAMILY)
        .fontSize($r('app.float.small_text_size'))
        .width(this.textWidth)
        .opacity(Constants.SECOND_LEVEL_OPACITY)
    }
    .onClick(() => {
      this.playingAnimation = true;
      setTimeout(() => {
        LottieUtil.loadAnimation({
          container: this.mainCanvasRenderingContext,
          renderer: 'canvas',
          loop: false,
          autoplay: false,
          name: this.articleId + this.type,
          path: this.isClicked ? this.cancelAnimationPath : this.clickAnimationPath
        }).addEventListener('complete', () => {
          this.playingAnimation = false;
          LottieUtil.destroy();
        })
        LottieUtil.play();

        this.isClicked = !this.isClicked;
        this.onClicked?.();
      }, Constants.DELAY_TIME);
    })
    .width($r('app.float.action_button_width'))
    .padding({ top: $r('app.float.sm_padding_margin') })
    .alignItems(VerticalAlign.Center)
    .justifyContent(FlexAlign.Center)
    .id('row' + this.articleId + this.type)
  }
}

