import {getBgColor, getButtonColor, getFontColor, getViewType } from '../../common/common'
import { getButtonPadding, getButtonFontSize, getButtonHeight} from '../../common/common'
import { GeekUIViewSize, GeekUIViewType } from '../../common/typying'
import {GeekUICommonEvent} from '../../common/event'
import {BITTON_RADIUS_DEFAULT} from '../../common/constrant'
/**
 * 自定义按钮
 */
export interface IButtonOptions {
  label?: ResourceStr,
  // 按钮类型
  type?: GeekUIViewType,
  // 朴素按钮
  plain?: boolean,
  // 胶囊按钮
  round?: boolean,
  // 圆形按钮
  circle?: boolean,
  // 圆角
  radius?: number,
  // 尺寸
  size?: GeekUIViewSize,
  // 宽
  width?: Length,
  // 高
  height?: Length,
  // 禁用
  disabled?: boolean,
  // 是否有交互
  stateEffect?: boolean,
  // 事件
  event?: GeekUICommonEvent,
  // 颜色
  color?: ResourceColor;
}
@Builder
function GeekUIButton($$: IButtonOptions){
  Button($$.label, {
      type: getSysButtonType($$.circle, $$.round),
      stateEffect: $$.stateEffect
    })
    .backgroundColor(getBgColor($$.type, $$.plain, $$.color))
    .borderRadius(!$$.radius && $$.radius !== 0 ? BITTON_RADIUS_DEFAULT : $$.radius)
    .borderColor($$.color ? $$.color : getButtonColor($$.type))
    .borderWidth(1)
    .fontColor(getFontColor($$.type, $$.plain, $$.color))
    .padding(getButtonPadding($$.size))
    .fontSize(getButtonFontSize($$.size))
    .height($$.height ? $$.height : getButtonHeight($$.size))
    .width($$.width)
    .registenerEvent($$.event)
    .enabled(!$$.disabled)
}



function getSysButtonType(circle: boolean, round: boolean){
  if (circle) return ButtonType.Circle
  if (round) return ButtonType.Capsule
  else return ButtonType.Normal
}


@Extend(Button)
function registenerEvent(options?: GeekUICommonEvent){
  .onClick((event: ClickEvent) => {options && options.onClick && options.onClick(event)})
  .onHover((isHover: boolean) => {options && options.onHover && options.onHover(isHover)})
  .onMouse((event: MouseEvent) => {options && options.onMouse && options.onMouse(event)})
  .onTouch((event: TouchEvent) => {options && options.onTouch && options.onTouch(event)})
  .onKeyEvent((event: KeyEvent) => {options && options.onKeyEvent && options.onKeyEvent(event)})
  .onFocus(() => {options && options.onFocus && options.onFocus()})
  .onBlur(() => {options && options.onBlur && options.onBlur()})
  .onAppear(() => {options && options.onAppear && options.onAppear()})
  .onDisAppear(() => {options && options.onDisAppear && options.onDisAppear()})
  .onAreaChange((oldValue: Area, newValue: Area) => {options && options.onAreaChange && options.onAreaChange(oldValue, newValue)})
  .onDragStart((event?: DragEvent, extraParams?: string) => {options && options.onDragStart && options.onDragStart(event)})
  .onDragEnter((event?: DragEvent, extraParams?: string) => {options && options.onDragEnter && options.onDragEnter(event)})
  .onDragMove((event?: DragEvent, extraParams?: string) => {options && options.onDragMove && options.onDragMove(event)})
  .onDragLeave((event?: DragEvent, extraParams?: string) => {options && options.onDragLeave && options.onDragLeave(event)})
  .onDrop((event?: DragEvent, extraParams?: string) => {options && options.onDrop && options.onDrop(event)})
  // .onVisibleAreaChange((ratios: Array<number>, isVisible: boolean, currentRatio: number) => {})
}
export {GeekUIButton}