# 实用工具库

## 简介

提供给开发者日志，弹窗，判断的工具类

## 下载安裝

```
 ohpm install @ohos/libgeekapp
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

1.在相应的类中引入组件：

```
import libgeekapp from '@ohos/libgeekapp'
```

2.相关使用：

```
aboutToAppear(): void {
Logger.info('sss', 'ssss');
if (TextUtil.isEmpty('sss')) {

    }
    showToast('测试');
}
```

## 约束与限制

在下述版本验证通过：
- DevEco Studio: NEXT Developer Preview2(4.1.3.700), SDK: API11(4.1.0(11))

## 目录结构

````
/libgeekapp    # 项目根目录
├── entry      # 示例代码文件夹
├── components     # components
│    └─ Logger
│     TextUtil                
├── README.md  # 安装使用方法                    
````

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/geekharmony/hmosapp1/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/geekharmony/hmosapp1/pulls) 。

## 开源协议

本项目基于 [Apache License](https://gitee.com/geekharmony/hmosapp1/blob/master/libgeekapp/LICENSE) ，请自由地享受和参与开源。

