/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Video1Controller } from '../controller/Video1Controller';
import { Video1CommonConstants } from '../common/constants/Video1CommonConstants';
import { Video1PlayConstants } from '../common/constants/Video1PlayConstants';

@Component
export struct Video1PlayControl {
  @Consume status: number;
  private playVideoModel?: Video1Controller;

  build() {
    Column() {
      Row() {
        Image($r("app.media.ic_video1_previous"))
          .width($r('app.float.control_image_width'))
          .aspectRatio(Video1CommonConstants.ASPECT_RATIO)
          .onClick(async () => {
            if (this.playVideoModel !== null) {
              this.playVideoModel!.previousVideo();
            }
            this.status = Video1CommonConstants.STATUS_START;
          })
        Column() {
          Image(this.status === Video1CommonConstants.STATUS_START ?
          $r("app.media.ic_video1_pause") : $r("app.media.ic_video1_play"))
            .width($r('app.float.control_image_width'))
            .aspectRatio(Video1CommonConstants.ASPECT_RATIO)
            .onClick(async () => {
              if (this.playVideoModel !== null) {
                let curStatus = (this.playVideoModel!.getStatus() === Video1CommonConstants.STATUS_START);
                this.status = curStatus ? Video1CommonConstants.STATUS_PAUSE : Video1CommonConstants.STATUS_START;
                this.playVideoModel!.switchPlayOrPause();
              }
            })
        }
        .layoutWeight(1)

        Image($r("app.media.ic_video1_next"))
          .width($r('app.float.control_image_width'))
          .aspectRatio(Video1CommonConstants.ASPECT_RATIO)
          .onClick(() => {
            if (this.playVideoModel !== null) {
              this.playVideoModel!.nextVideo();
            }
            this.status = Video1CommonConstants.STATUS_START;
          })
      }
      .width(Video1PlayConstants.CONTROL_ROW_WIDTH)
    }
    .width(Video1CommonConstants.FULL_PERCENT)
    .height(Video1CommonConstants.FULL_PERCENT)
    .justifyContent(FlexAlign.Center)
  }
}