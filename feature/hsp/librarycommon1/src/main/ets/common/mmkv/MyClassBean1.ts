/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { SerializeBase, Serialize } from '@ohos/MMKV';

export class MyClassBean1 extends SerializeBase {
  @Serialize()
  public code: number = 0;
  public title: string = 'titles';
  @Serialize('desc')
  public description: string = 'descs';

  constructor(code?: number, title?: string, des?: string) {
    super();
    if (!!code) {
      this.code = code
    }
    if (!!title) {
      this.title = title
    }
    if (!!des) {
      this.description = des
    }
  }
}