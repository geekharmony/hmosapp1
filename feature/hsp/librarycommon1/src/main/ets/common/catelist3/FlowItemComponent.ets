/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { waterFlowData } from './HomeViewModel';
import ProductItem from './ProductItem';


/**
 * Flow item component.
 * Usage: This component is used in FlowItem.
 */
@Component
export default struct FlowItemComponent {
  item: ProductItem = waterFlowData[0];

  build() {
    Column() {
      Image(this.item?.image_url)
        .width(132)
        .height(132)
        .objectFit(ImageFit.Contain)
        .margin({
          top: 12,
          bottom: 8
        })
      Text(this.item?.name)
        .fontSize(14)
        .fontColor(Color.Black)
        .fontWeight(FontWeight.Normal)
        .alignSelf(ItemAlign.Start)
      Text(this.item?.discount)
        .fontSize(12)
        .fontColor(Color.Black)
        .fontWeight(FontWeight.Normal)
        .opacity(0.6)
        .alignSelf(ItemAlign.Start)
        .margin({
          bottom: 4
        })
      Text(this.item?.price)
        .fontSize(16)
        .fontColor('#E92F4F')
        .fontWeight(FontWeight.Normal)
        .alignSelf(ItemAlign.Start)
        .lineHeight(19)
      // Row() {
      //   if (this.item?.promotion) {
      //     Text(`${this.item?.promotion}`)
      //       .height(16)
      //       .fontSize(10)
      //       .fontColor(Color.White)
      //       .borderRadius(4)
      //       .backgroundColor('#E92F4F')
      //       .padding({
      //         left: 4,
      //         right: 4
      //       })
      //       .margin({
      //         top: 6,
      //         right: 8
      //       })
      //   }
      //   if (this.item?.bonus_points) {
      //     Text(`${this.item?.bonus_points}`)
      //       .height(16)
      //       .fontSize(16)
      //       .fontColor('#E92F4F')
      //       .borderRadius(4)
      //       .borderWidth(0.5)
      //       .borderColor('#E92F4F')
      //       .padding({
      //         left: 8,
      //         right: 8
      //       })
      //       .margin({ top: 6 })
      //   }
      // }
      // .width('100%')
      // .justifyContent(FlexAlign.Start)
    }
    .borderRadius(8)
    .backgroundColor(Color.White)
    .padding({
      left: 12,
      right: 12,
      bottom: 12
    })
  }
}