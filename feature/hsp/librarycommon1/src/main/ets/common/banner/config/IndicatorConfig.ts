/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import image from '@ohos.multimedia.image';
import { BannerConfig } from './BannerConfig'



export class IndicatorConfig {


  // pixelmap
  selectPixelMap:image.PixelMap = undefined // 选中pixelmap PixelMapIndicator中使用
  normalPixelMap:image.PixelMap = undefined // 未选中pixelmap PixelMapIndicator中使用
  indicatorSize: number = 3; // 一共有多少个元素
  currentPosition: number = 0; // 当前第几个元素
  indicatorSpace: number = BannerConfig.INDICATOR_SPACE; // 指示器点间距
  normalWidth: number = BannerConfig.INDICATOR_NORMAL_WIDTH; // 未选中长度
  selectedWidth: number = BannerConfig.INDICATOR_SELECTED_WIDTH; // 选中长度
  normalColor: string = BannerConfig.INDICATOR_NORMAL_COLOR; // 未选中颜色
  selectedColor: string = BannerConfig.INDICATOR_SELECTED_COLOR; // 选中颜色
  radius: number = BannerConfig.INDICATOR_RADIUS; // 圆角 roundLineIndicator中使用
  height: number = BannerConfig.INDICATOR_HEIGHT; // 指示器高度 在RectFIndicator中使用

  public constructor(indicatorSize:number, currentPosition:number,selectColor:string, normalColor:string){
    this.indicatorSize = indicatorSize;
    this.currentPosition = currentPosition;
    this.selectedColor = selectColor;
    this.normalColor = normalColor;
  }

  getIndicatorSize() {
    return this.indicatorSize;
  }

  setIndicatorSize(indicatorSize: number) {
    this.indicatorSize = indicatorSize;
    return this;
  }

  getNormalColor() {
    return this.normalColor;
  }

  setNormalColor(normalColor: string) {
    this.normalColor = normalColor;
    return this;
  }

  getSelectedColor() {
    return this.selectedColor;
  }

  setSelectedColor(selectedColor: string) {
    this.selectedColor = selectedColor;
    return this;
  }

  getIndicatorSpace() {
    return this.indicatorSpace;
  }

  setIndicatorSpace(indicatorSpace: number) {
    this.indicatorSpace = indicatorSpace;
    return this;
  }

  getCurrentPosition() {
    return this.currentPosition;
  }

  setCurrentPosition(currentPosition: number) {
    this.currentPosition = currentPosition;
    return this;
  }

  getNormalWidth() {
    return this.normalWidth;
  }

  setNormalWidth(normalWidth) {
    this.normalWidth = normalWidth;
    return this;
  }

  getSelectedWidth() {
    return this.selectedWidth;
  }

  setSelectedWidth(selectedWidth) {
    this.selectedWidth = selectedWidth;
    return this;
  }



  getRadius() {
    return this.radius;
  }

  setRadius( radius) {
    this.radius = radius;
    return this;
  }

  getHeight() {
    return this.height;
  }

  setHeight(height) {
    this.height = height;
    return this;
  }

  setSelectPixelMap(select){
    this.selectPixelMap = select
    return this;
  }

  getSelectPixelMap(){
    this.selectPixelMap
  }

  setNormalPixelMap(normal){
    this.normalPixelMap = normal
    return this
  }
  getNormalPixelMap(){
    return this.normalPixelMap
  }
}
