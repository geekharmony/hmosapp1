/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AnimatedEnum } from '../transformer/AnimatedEnum'
import {SwiperIndicator,BannerMargin,AnimatedConfig} from './Banner'
export class BannerOptions{
  swiperController:SwiperController = new SwiperController();
  autoPlay?:boolean = false // 自定播放
  interval?:number = 3000 // 当前item停滞时间
  indicator?:SwiperIndicator = { bool: false} // 系统的指示器配置 不启用设置false
  loop?:boolean = true // 是否开启循环
  duration?:number = 400 // 子组件切换动画时长
  vertical?:boolean = false // 是否纵向滑动
  curve?:Curve = Curve.Linear // 动画曲率
  disableSwipe?:boolean = false; // 禁止触摸

  bannerMargin?:BannerMargin = {left:0, right:0};// 水平是左右的margin,垂直为上下的margin
  animatedEnum?:AnimatedEnum = AnimatedEnum.NonePage// 动画效果
  animatedConfig?:AnimatedConfig = {}

  onChange?: (index:number)=>void = undefined; // swiper当前index回调
  onGestureSwipe?:(index: number, extraInfo:SwiperAnimationEvent)=>void = undefined // swiper手势回调
  onAnimationStart?:(index: number, targetIndex: number, extraInfo:SwiperAnimationEvent)=>void = undefined // swiper动画开始回调
  onAnimationEnd?:(index: number, extraInfo:SwiperAnimationEvent)=>void = undefined // swiper动画结束回调

}