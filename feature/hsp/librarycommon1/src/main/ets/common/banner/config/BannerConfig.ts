/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class BannerConfig {
  static IS_AUTO_LOOP: boolean = true;
  static IS_INFINITE_LOOP: boolean = true;
  static LOOP_TIME: number = 3000;
  static SCROLL_TIME: number = 600;
  static INCREASE_COUNT: number = 2;
  static INDICATOR_NORMAL_COLOR: string = '#88ffffff';
  static INDICATOR_SELECTED_COLOR: string = '#88000000';
  static INDICATOR_NORMAL_WIDTH: number = 5;
  static INDICATOR_SELECTED_WIDTH: number = 7;
  static INDICATOR_SPACE: number = 5;
  static INDICATOR_MARGIN: number = 5;
  static INDICATOR_HEIGHT: number = 3;
  static INDICATOR_RADIUS: number = 1.5;
}
