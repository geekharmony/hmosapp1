/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export enum AnimatedEnum{
  NonePage = 0, // 无动画效果
  AlphaPage = 1, // alpha渐变
  DepthPage = 2, // 叠层
  MZScaleIn = 3, // 魅族效果 通常配合画廊
  RotateDownPage = 4, // 圆形旋转翻页 中心点在下方
  RotateUpPage = 5, // 圆形旋转翻页 中心点在上方
  RotateY = 6, // 3D旋转翻页 旋转轴Y
  ScaleIn = 7, // 缩放进入
  ZoomOutPage = 8 // 缩放alpha进入
}