/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { webview } from '@kit.ArkWeb';
import { router } from '@kit.ArkUI';
import { common } from '@kit.AbilityKit';
import { Route, WebViewConstants } from '../../constants/WebViewConstants';
import startAbilityModel from './StartAbilityModel';
import { resourceManager } from '@kit.LocalizationKit';
import { showToast } from '@ohos/libutils/Index';

@Entry
@Component
struct WebComponent {
  @State data: Route = (router.getParams() as Route);
  context: common.UIAbilityContext = getContext(this) as common.UIAbilityContext;
  controller: webview.WebviewController = new webview.WebviewController();
  // private rawfileFd: Resource | undefined = undefined;
  // private rawfileFd: RawFileDescriptor | string = '';
  // url111?: string = '';
  private url111?: resourceManager.RawFileDescriptor;

  async onPageShow(): Promise<void> {
    // hsp har 获取raw
    // let context2 = getContext(this).createModuleContext('libwebview') as common.UIAbilityContext;
    // let manager = context2.resourceManager;
    // let rawfileFd = await manager.getRawFd(this.data.html).catch((error: BusinessError) => {
    //   Logger.error(`resourceManager error code ${error.code} message ${error.message}`);
    // })
    // if (rawfileFd) {
    //   this.url111 = rawfileFd;
    // }
  }

  build() {
    Column() {
      Column() {
        Text(this.data.text)
          .fontSize(24)
          .textAlign(TextAlign.Start)
          .fontWeight(WebViewConstants.FONT_WEIGHT_SEVEN)
          .fontColor('#000000')
          .width(WebViewConstants.FULL_WIDTH)
          .onClick(() => {
            // 调用h5中的方法
            this.controller.runJavaScript('startDraw()');
          })
      }
      .width(WebViewConstants.FULL_WIDTH)
      .padding({
        top: WebViewConstants.PADDING_VALUE_TWELVE,
        bottom: WebViewConstants.PADDING_VALUE_TWELVE,
        left: WebViewConstants.PADDING_VALUE_TWENTY_FOUR,
        right: WebViewConstants.PADDING_VALUE_TWENTY_FOUR
      })

      Column() {
        // Web({ src: $rawfile(this.data.html), controller: this.controller })
        Web({ src: this.data.html, controller: this.controller })
          .onLoadIntercept((event) => {
            if (event) {
              let url = event.data.getRequestUrl();
              if (url.indexOf(WebViewConstants.NATIVE_CUR) === 0) {
                router.pushUrl({
                  url: WebViewConstants.HMOS_PAGENAME + url.substring(WebViewConstants.NATIVE_CUR.length)
                })
                return true;
              }
              else if (url.indexOf(WebViewConstants.NATIVE_NEW) === 0) {
                startAbilityModel.startSecondAbility(this.context)
                return true;
              }
              else if (url.indexOf(WebViewConstants.OTHER_APP) === 0) {
                startAbilityModel.startThirdPartyAppAbility(this.context, url.substring(WebViewConstants.OTHER_APP.length));
                return true;
              }
              else if (url.indexOf(WebViewConstants.HMOS) === 0) {
                startAbilityModel.startSettingsInfo(this.context, url.substring(WebViewConstants.HMOS.length))
                return true;
              }
              else if (url.indexOf(WebViewConstants.DEVICE) === 0) {
                startAbilityModel.startCrossDeviceAbility(this.context, url.substring(WebViewConstants.DEVICE.length));
                return true;
              } else {
                return false;
              }
            }
            return false;
          })
          .javaScriptAccess(true)// h5调用原生方法
          .javaScriptProxy({
            object: this.linkObj,
            name: 'linkObj',
            methodList: ['messageFromHtml'],
            controller: this.controller
          })
          .width(WebViewConstants.FULL_WIDTH)
          .height(WebViewConstants.FULL_HEIGHT)
      }
      .padding({ left: WebViewConstants.PADDING_VALUE_SIXTEEN, right: WebViewConstants.PADDING_VALUE_SIXTEEN })
    }
    .padding({ top: AppStorage.get<number>('statusBarHeight') })
  }

  @State linkObj: LinkClass = new LinkClass();
}

class LinkClass {
  messageFromHtml(value: string) {
    // AlertDialog.show({
    //   message: '刚进入页面的时候去调用原生数据然后再加载出网页',
    //   confirm: {
    //     value: '确定',
    //     action: () => {
    //     }
    //   },
    //   cancel: () => {
    //   }
    // });
    showToast('刚进入页面的时候去调用原生方法然后再加载出网页')
  }
}






