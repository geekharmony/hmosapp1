/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Using AGC Functions to Obtain Network Data.
 */
import { BusinessError } from '@kit.BasicServicesKit';
import { Request } from '@ohos/libgeekapp/Index';
import { Logger, ResourcesType, ResponseData } from '@ohos/libgeekapp/Index';
import { ListView1Model1 } from '../model/ListView1Model1';
import { ListView1Model2 } from '../model/ListView1Model2';

import { GetMoreResourcesParams, UserIdParams } from './ListView1Params';
import { ListView1Trigger } from './ListView1Trigger';

const TAG = '[DiscoverNetFunc]';

const RESOURCE_PAGE_SIZE = 10;

export class ListView1NetFunc {
  /**
   * @returns NetworkNewsResources
   */
  public getHomeResources(): Promise<ListView1Model2> {
    const params: UserIdParams = {
      userId: AppStorage.get<string>('userId') as string
    };

    return new Promise((resolve: (value: ListView1Model2 | PromiseLike<ListView1Model2>) => void,
                        reject: (reason?: Object) => void) => {
      Request.call(ListView1Trigger.HOME_RESOURCE, params).then((result: Object) => {
        Logger.info(TAG, 'getHomeResources success--------' + JSON.stringify(result));
        resolve(result as ListView1Model2);
      }).catch((error: BusinessError) => {
        Logger.error(TAG, 'getHomeResources error--------' + JSON.stringify(error));
        reject(error);
      });
    });
  }

  /**
   * @param pageNum
   * @param type
   * @returns ResourcesData
   */

  public getMoreResources(pageNum: number, type: ResourcesType): Promise<ResponseData<ListView1Model1>> {
    const params: GetMoreResourcesParams = {
      userId: AppStorage.get<string>('userId') as string,
      type,
      pageNum,
      pageSize: RESOURCE_PAGE_SIZE
    };

    return new Promise((resolve: (value: ResponseData<ListView1Model1> | PromiseLike<ResponseData<ListView1Model1>>) => void,
                        reject: (reason?: Object) => void) => {
      Request.call(ListView1Trigger.RESOURCE_PAGE, params).then((resourcesData: Object) => {
        Logger.info(TAG, 'getMoreResources success--------' + JSON.stringify(resourcesData));
        resolve(resourcesData as ResponseData<ListView1Model1>);
      }).catch((error: BusinessError) => {
        Logger.error(TAG, 'getMoreResources error--------' + JSON.stringify(error));
        reject(error);
      });
    });
  }
}