# TabLayout

**专门为OpenHarmony打造的一款TabLayout库，希望各位用得开心**

## 效果图

![2024-04-24_09-22-56 (1)](https://zyc-essay.oss-cn-beijing.aliyuncs.com/share/2024-05-20_13-39-20%20(2).gif)

## 下载安装

```
ohpm install @zyc/tablayout
```

## 用例

见entry目录[Index.ets](..%2Fentry%2Fsrc%2Fmain%2Fets%2Fpages%2FIndex.ets)

## 使用说明

```typescript
TabLayout({
  tabs: this.tabsInfo,
  gapContinuousAnimation: false,
  smartToIndex: this.smartIndex,
  indicatorStyle: {
    warpTab: true,
    type: "line",
    colorGradient: {
      angle: 90,
      direction: GradientDirection.Left,
      colors: [["#ff9a9e", 0], ["#fad0c4", 1]],
    }
  },
})
  .width("100%")
```

## 主要属性说明

| 参数名称                   | 入参内容                                   | 功能简介                   |
|------------------------|----------------------------------------|------------------------|
| tabStyle               | ITabStyle                              | Tab样式                  | 
| indicatorStyle         | IIndicatorStyle                        | 指示器样式                  |
| tabs                   | TabItem[]                              | 数据源                    |
| cacheCount             | number                                 | swiper缓存数量             |
| isOverPage             | boolean                                | Tab是否覆盖在Page上（抖音短视频效果） |
| gapContinuousAnimation | boolean                                | 间隔切换是否不做动画（失效）         |
| showIndicator          | boolean                                | 是否显示指示器                |
| smartToIndex           | number                                 | 主动切换指示器                |
| onTabSelected          | (position: number) => void             | Tab选中回调                |
| onTabItemBuilder       | (item: TabAttr) => void                | 自定义Tab                 |
| onTabIndicatorBuilder  | () => void                             | 自定义指示器                 |
| onContentBuilder       | (item: TabItem, index: number) => void | Page页面（不传则无滑动）         |
|                        |                                        |                        |

## Tab样式属性说明

| 参数名称                | 入参内容          | 功能简介                                 |
|---------------------|---------------|--------------------------------------|
| fontSizeNormal      | number        | 未选中文字大小                              | 
| fontSizeSelect      | number        | 选中文字大小                               | 
| fontColorNormal     | ResourceColor | 未选中文字颜色                              | 
| fontColorSelect     | ResourceColor | 选中文字颜色                               | 
| fontWeightNormal    | FontWeight    | 未选中文字权重                              | 
| fontWeightSelect    | FontWeight    | 选中文字权重                               | 
| tabPadding          | Padding       | tab child之间的距离                       | 
| tabAlignment        | Alignment     | tab位于屏幕的位置                           | 
| tabAverageEnable    | boolean       | 是否等分tab容器，必须搭配tabContainerWidth      | 
| tabContainerPadding | Padding       | tab容器的Padding，建议使用tabContainerMargin | 
| tabContainerMargin  | Padding       | tab容器的Margin                         | 
| tabContainerHeight  | Length        | tab容器的高度                             | 
| tabContainerWidth   | Length        | tab容器的宽度，默认为auto                     | 

| | | |

## Indicator样式属性说明

| 参数名称          | 入参内容                        | 功能简介                             |
|---------------|-----------------------------|----------------------------------|
| type          | "line" , "block" , "custom" | 指示器类型线条、背景块、自定义（自定义会导致绝大多数属性不生效） | 
| width         | number                      | 宽度                               |
| height        | number                      | 高度                               |
| animationType | "scroll" , "alpha"          | 切换动画                             |
| alignment     | Alignment                   | 相对于Tab的位置                        |
| radius        | number                      | 圆角                               |
| color         | ResourceColor               | 颜色                               |
| colorGradient | ResourceColor               | 渐变颜色                             |
| warpTab       | boolean                     | 自适应Tab宽度                         |
| margin        | Margin , Length             | 不受warpTab限制                      |
| padding       | Margin , Length             | 不受warpTab限制                      |
## 已知问题
代码在API12 中，Swiper onChange回调非常的靠后，指示器和Tab选中间隔会有很明显的延迟感。API11没有该问题。
Demo目前只能运行在API12中，API11 需自行修改。
## 贡献代码

使用过程中发现任何问题都可以提[Issue](https://gitee.com/AlbertZyc/tab-layout/issues)
给我们，当然，我们也非常欢迎你给我们提[PR](https://gitee.com/AlbertZyc/tab-layout/pulls) 。

## 交流
当然你也可以加我微信交流，记得备注来源～
![img.png](img%2Fimg.png)