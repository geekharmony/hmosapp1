# hptdialog

___

# 简介

___

`hptdialog` 一款极简的弹框库，基于自定义弹框`CustomDialog`进行封装，并简化了其调用方式，帮助开发者快速实现自定义的弹框效果，目前包含
`hptdialog` 和 `hptBottomSheet`

# 下载安装

___
`ohpm install @gancao/hptdialog`

# 接口与参数说明

___

- ## `hptdialog`

___

#### 接口

| **方法名**            | **入参**                                                                                                        | **描述** |
|--------------------|---------------------------------------------------------------------------------------------------------------|--------|
| HPTDialogUtil.show | component: Object 当前所属页面的this <br/> info: DialogInfo 显示的内容包括title、message等 <br/>config?: DialogConfig 弹框的样式配置 | 显示一个弹框 |

#### `DialogInfo` 属性（描述弹框的内容）

| 属性名                           | 类型                                        | 描述                                                                 |
|-------------------------------|-------------------------------------------|--------------------------------------------------------------------|
| `title`                       | `ResourceStr`                             | 弹框标题，可选                                                            |
| `message`                     | `ResourceStr`                             | 弹框消息内容，可选                                                          |
| `messageContent`              | `CustomBuilder`                           | 自定义中间内容，用于覆盖`message`字段。注意：`messagePadding`对此字段无效                  |
| `buttons`                     | `DialogButton[]`                          | 当需要多个按钮时使用，可以覆盖默认的取消和确认按钮； <br/>DialogButton 类型见 DialogButton 参数说明 |
| `cancel`                      | `ResourceStr`                             | 取消按钮文字，默认为“取消”                                                     |
| `confirm`                     | `ResourceStr`                             | 确认按钮文字，默认为“确定”                                                     |
| `cancelOnClick`               | `(controller?: DialogController) => void` | 取消按钮点击回调函数，通过`controller`可以控制是否关闭弹框                                |
| `confirmOnClick`              | `(controller?: DialogController) => void` | 确认按钮点击回调函数，同上，根据`controller`控制关闭逻辑                                 |
| `disableClickButtonAutoClose` | `boolean`                                 | 是否禁止点击按钮后自动关闭弹框，默认自动关闭                                             |
| `cancelCallBack`              | `() => void`                              | 弹框取消时的回调函数，与点击取消按钮或关闭图标等操作相关联                                      |

#### `DialogConfig` 属性（描述弹框的样式）

| 属性名                             | 类型                              | 描述                                                      |
|---------------------------------|---------------------------------|---------------------------------------------------------|
| `margin`                        | `Margin`                        | 距屏幕的间距                                                  |
| `padding`                       | `Padding`                       | 内间距                                                     |
| `border`                        | `BorderOptions`                 | 边框（圆角）                                                  |
| `backgroundColor`               | `ResourceColor`                 | 内容背景颜色                                                  |
| `fontSize`                      | `number 或 string 或 Resource`    | 标题字体大小                                                  |
| `fontColor`                     | `ResourceColor`                 | 标题字体颜色                                                  |
| `fontWeight`                    | `number 或 FontWeight 或 string`  | 字体粗细                                                    |
| `titlePadding`                  | `Padding`                       | 标题区域的内边距                                                |
| `messageFontSize`               | `number 或  string 或 Resource`   | 内容字体大小                                                  |
| `messageFontColor`              | `ResourceColor`                 | 内容字体颜色                                                  |
| `messageFontWeight`             | `number 或  FontWeight 或 string` | 内容字体粗细                                                  |
| `messagePadding`                | `Padding`                       | 内容区域的内边距                                                |
| `buttonsUseVerticalLayoutCount` | `number`                        | 按钮改为垂直布局时的数量，默认小于此数量时，采用Row横向布局，如果大于等于此数量时，换成Column纵向布局 |
| `buttonVerticalSpacing`         | `string 或 number`               | 按钮垂直间距                                                  |
| `buttonHorizontalSpacing`       | `string 或 number`               | 按钮水平间距                                                  |

#### `DialogButton` 属性（弹框按钮）

| 属性名        | 类型                                        | 描述                                 |
|------------|-------------------------------------------|------------------------------------|
| `name`     | `string`                                  | 按钮的文本名称                            |
| `style?`   | `DialogButtonStyle`                       | 按钮的样式配置，可选                         |
| `onClick?` | `(controller?: DialogController) => void` | 按钮点击时触发的回调函数，可接收一个自定义对话框控制器作为参数，可选 |

#### `DialogButtonStyle` 属性（弹框按钮样式）

| 属性名               | 类型                             | 描述                    |
|-------------------|--------------------------------|-----------------------|
| `height`          | `Length`                       | 按钮的高度设置               |
| `fontSize`        | `number 或 string 或 Resource`   | 文本的字体大小，支持数字、字符串或资源引用 |
| `fontColor`       | `ResourceColor`                | 文本的颜色，通过资源定义          |
| `fontWeight`      | `number 或 FontWeight 或 string` | 文本的字体粗细               |
| `backgroundColor` | `ResourceColor`                | 按钮的背景颜色               |
| `border`          | `BorderOptions`                | 按钮边框的样式设置             |

- ## `hptBottomSheet`

___

#### 接口

| **方法名**                 | **入参**                                                                                                                      | **描述**   |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------|----------|
| HPTBottomSheetUtil.show | component: Object 当前所属页面的this <br/> info: BottomSheetInfo 显示的内容包括title、contentList等 <br/>config?: BottomSheetConfig 弹框的样式配置 | 显示一个底部弹框 |

#### `BottomSheetInfo` 属性（描述弹框的内容）

| 属性名                           | 类型                             | 描述                                                                 |
|-------------------------------|--------------------------------|--------------------------------------------------------------------|
| `title`                       | `ResourceStr`                  | 底部弹窗的标题                                                            |
| `contentList`                 | `ResourceStr[]`                | 以列表形式展示的内容，默认不提供                                                   |
| `selectedIndex`               | `number`                       | 默认选中的行索引                                                           |
| `customContent`               | `CustomBuilder`                | 自定义内容，用于覆盖`contentList`字段                                          |
| `cancel`                      | `ResourceStr`                  | 取消按钮的文字，默认为“取消”                                                    |
| `disableShowCancel`           | `boolean`                      | 是否禁止显示取消按钮，默认不禁止                                                   |
| `cancelOnClick`               | `(controller?) => void`        | 取消按钮点击回调函数，接收一个可选的`DialogController`参数，用于控制弹窗关闭逻辑                  |
| `confirmOnClick`              | `(index, controller?) => void` | 选中行的回调函数，接收行的索引`index`和一个可选的`DialogController`参数，用于执行相应操作并控制弹窗关闭逻辑 |
| `disableClickButtonAutoClose` | `boolean`                      | 是否禁止点击按钮后自动关闭弹窗，默认自动关闭                                             |
| `cancelCallBack`              | `() => void`                   | 弹窗被取消时的回调函数，与点击取消按钮或其它取消操作关联                                       |

#### `BottomSheetConfig` 属性（描述弹框的样式）

| 属性名                           | 类型                             | 描述                |
|-------------------------------|--------------------------------|-------------------|
| `backgroundColor`             | `ResourceColor`                | 弹窗的背景颜色           |
| `borderRadius`                | `number`                       | 底部弹窗的圆角大小         |
| `maxListHeight`               | `number`                       | 列表选项模式下，弹窗的最大显示高度 |
| `itemHeight`                  | `number`                       | 每个列表项的高度          |
| `itemBackgroundColor`         | `ResourceColor`                | 列表项的背景颜色          |
| `itemSelectedBackgroundColor` | `ResourceColor`                | 选中列表项的背景颜色        |
| `dividerColor`                | `ResourceColor`                | 分割线颜色             |
| `dividerHeight`               | `Length`                       | 分割线高度             |
| `cancelSpace`                 | `Length`                       | 取消按钮与其它元素之间的间距    |
| `cancelHeight`                | `Length`                       | 取消按钮的高度           |
| `titleAlign`                  | `TextAlign`                    | 标题的对齐方式           |
| `titlePadding`                | `Padding`                      | 标题区域的内边距          |
| `titleFontSize`               | `number 或 string 或 Resource`   | 标题的字体大小           |
| `titleFontColor`              | `ResourceColor`                | 标题的字体颜色           |
| `titleFontWeight`             | `number 或 FontWeight 或 string` | 标题的字体粗细           |
| `fontSize`                    | `number 或 string 或 Resource`   | 文字的字体大小           |
| `fontColor`                   | `ResourceColor`                | 文字的颜色             |
| `fontWeight`                  | `number 或 FontWeight 或 string` | 文字的字体粗细           |

# 修改全局默认配置

```typescript
HPTDialogUtil.defaultConfig.titlePadding = { bottom: 24 }
HPTDialogUtil.defaultConfig.fontSize = $r("app.float.h3")
HPTDialogUtil.defaultConfig.fontColor = $r("app.color.mainText")
HPTDialogUtil.defaultConfig.messageFontSize = 12
HPTDialogUtil.defaultConfig.messageFontColor = Color.Red

// BottomSheet
HPTBottomSheetUtil.defaultConfig.itemSelectedBackgroundColor = $r("app.color.primary")
```

# 效果展示
___

![效果](./demo_photo/Screenshot_2024-06-06T092239.png)

![效果](./demo_photo/Screenshot_2024-06-06T092301.png)

# 使用示例

___

### HPTDialog

```typescript
HPTDialogUtil.show(this, {
  title: '是否确认退出登录？',
  cancel: "取消",
  message: "退出说明xxxxx",
  confirm: "确认退出",
  confirmOnClick: () => {
    // 执行退出登录逻辑
  }
})
```

#### 自定义

```typescript
HPTDialogUtil.show(this, {
  title: '是否确认退出登录？',
  cancel: "取消",
  confirm: "确认退出",
  messageContent: () => this.buildItem("退出说明xxxxx", () => {

  }),
  confirmOnClick: () => {

  }
})
```

### HPTBottomSheet

```typescript
HPTBottomSheetUtil.show(this, {
  contentList: ['代煎', '不代煎', '每次手动选择'],
  disableShowTitle: true
})
```

# 注意️⚠️

`this` 是之当前的`@Component`，如果在逻辑层代码调用弹框，需要向逻辑层方法传入当前的`@Component`的`this`，否则弹框可能无法正常弹出

# 目录结构

```
hptdialog
├── src
│   ├── main
│   │   │── ets
│   │   │     └── components
│   │   │         └── dialog
│   │   │              └── Dialog.ets
│   │   │              └── Config.ets     
│   │   │         └── bottomsheet
│   │   │              └── BottomSheet.ets
│   │   │              └── Config.ets
│   │   │         └── DialogController.ets
│   │   └── resources
└── 
```

# 项目地址
[仓库地址](https://gitee.com/iJianbao/gancao-hptdialog.git)
