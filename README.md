# 实用工具库1

## 简介

提供给开发者日志，弹窗，判断的工具类,相当牛逼

![003.gif](libgeekapp/src/img/003.gif)

[//]: # (![点击查看]&#40;http://cdn2.cdn.haier-jiuzhidao.com/dibu1/VID_1719995486_007.gif&#41;)

[//]: # (<video width="320" height="640" controls autoplay muted loop> )

[//]: # (  <source src="/src/img/001.mp4" type="video/mp4"> )

[//]: # (</video> )

[//]: # (<video src="/src/img/001.mp4"></video>)
## 下载安裝

```
 ohpm install @geekapp/libgeekapp
```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

1.在相应的类中引入组件：

```
import libgeekapp from '@ohos/libgeekapp'
```
```
    "@hw-agconnect/cloud": "1.0.2",
    "@hw-agconnect/hmcore": "1.0.2",
    "long": "5.2.1",
    "@ohos/lottie": "^2.0.10",
    "@ohos/imageknife": "^2.1.2",
    "@ohos/banner": "^1.0.1-rc.5",
    "@ohos/httpclient": "^2.0.0",
    "@pura/harmony-utils": "^1.0.0",
    "@hzw/ohos-dsbridge": "^1.5.1",
    "@zyc/tablayout": "^1.0.10",
    "@gancao/hptdialog": "^1.0.2"
```

2.相关使用：

```
import { abilityAccessCtrl, AbilityConstant, bundleManager, Permissions, UIAbility, Want } from '@kit.AbilityKit';
import { hilog } from '@kit.PerformanceAnalysisKit';
import { window } from '@kit.ArkUI';
import {
  CommonConstants,
  CommonSizeConstants,
  EmptyViewStateConfig,
  EmptyViewStateEnum,
  Logger,
  SizePreferencesUtil,
  WindowUtil
} from '@geekapp/libgeekapp';
import { ImageKnife, ImageKnifeGlobal } from '@ohos/imageknife';
// import { Bugly, BuglyBuilder } from 'bugly';
import { DeviceUtil } from '@pura/harmony-utils';

const TAG = '[EntryAbility]';
let localStorage: LocalStorage = new LocalStorage();

export default class EntryAbility extends UIAbility {
  onCreate(want: Want, launchParam: AbilityConstant.LaunchParam): void {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onCreate');
    this.init_commponet(want, launchParam);
  }

  onDestroy(): void {
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
  }

  onWindowStageCreate(windowStage: window.WindowStage): void {
    // Main window is created, set main page for this ability
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');
    this.init_onWindowStageCreate(windowStage);
    // windowStage.loadContent('pages/Index', (err) => {
    //   if (err.code) {
    //     hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', JSON.stringify(err) ?? '');
    //     return;
    //   }
    //   hilog.info(0x0000, 'testTag', 'Succeeded in loading the content.');
    // });
  }

  onWindowStageDestroy(): void {
    // Main window is destroyed, release UI related resources
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
  }

  onWindowStageRestore(windowStage: window.WindowStage): void {
    Logger.info(TAG, 'Ability onWindowStageRestore');
    this.init_onWindowStageRestore(windowStage);
  }

  onForeground(): void {
    // Ability has brought to foreground
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
    AppStorage.setOrCreate('currentColorMode', this.context.config.colorMode);
  }

  onBackground(): void {
    // Ability has back to background
    hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
  }


  // 组件初始化
  init_commponet(want: Want, launchParam: AbilityConstant.LaunchParam): void {
    // webview多进程bufen
    try {
      import('@kit.ArkWeb').then(module => {
        module.webview.WebviewController.initializeWebEngine();
      });
    } catch (err) {
      Logger.error(TAG, 'dynamicLoading ArkWeb error:' + err);
    }
    // 设置字体默认大小
    SizePreferencesUtil.getInstance().createFontPreferences(this.context);
    SizePreferencesUtil.getInstance().saveDefaultFontSize(CommonSizeConstants.SET_SIZE_SMALL);
    //
    WindowUtil.setMissionContinueActive(this.context, false);
    // glide
    // 初始化全局ImageKnife
    ImageKnife.with(this.context);
    let imageKnife: ImageKnife | undefined = ImageKnifeGlobal.getInstance().getImageKnife()
    if (imageKnife != undefined) {
      //设置全局内存缓存大小张数
      imageKnife.setLruCacheSize(100, 100 * 1204 * 1024)
    }
    // emptyview
    //全局配置缺省页的各种图标和文案
    let globalStateConfig = new EmptyViewStateConfig()
    globalStateConfig.progressColor = Color.Red
    globalStateConfig.emptyStr = '我是全局配置的空白提示文案'
    globalStateConfig.emptyIcon = $r('app.media.state_empty')
    globalStateConfig.retryStr = '我是全局重试按钮'
    globalStateConfig.defaultState = EmptyViewStateEnum.LOADING
    globalStateConfig.showLoadingWhenRetry = false
    EmptyViewStateConfig.globalStateConfig = globalStateConfig
    // 网络请求
    // 登录保存数据
    // this.restoringData(want, launchParam)
    // bugly
    this.initBugly(this.context)

  }

  async initBugly(context: Context): Promise<void> {
    // let builder = new BuglyBuilder();
    //
    // builder.appId = '27c77de263';   // 必填，Bugly产品信息中的APP ID
    // builder.appKey = 'fd96bb31-4fcd-4e0f-9ae3-8950fdc46cdf';    // 必填，Bugly产品信息中的APP KEY
    // let id = await DeviceUtil.getDeviceId();
    // builder.deviceId = id;     // 必填，设备ID，应保证设备ID对不同设备唯一
    //
    // builder.appVersion = '1.1.0';   // 选填，业务的App版本
    // builder.buildNum = '110';         // 选填，业务App版本的构建号
    // builder.appChannel = 'website'; // 选填，业务App渠道
    // builder.userId = "12345";       // 选填，用户ID，如不设置则为空
    // let deviceType = DeviceUtil.getDeviceType()
    // // builder.deviceModel = "huawei"; // 选填，机型，如不设置则为空
    // builder.deviceModel = deviceType.toString(); // 选填，机型，如不设置则为空
    // builder.debugMode = true;       // 选填，默认开启，开启后Bugly SDK会打印更多调试日志，线上版本可关闭
    // builder.initDelay = 0;          // 选填，延迟初始化时间，单位ms
    //
    // let initResult = Bugly.init(context, builder);
    // Bugly.testCrash(Bugly.JS_CRASH); // 模拟Js异常
    // Bugly.testCrash(Bugly.CPP_CRASH); // 模拟native异常

  }

  init_onWindowStageCreate(windowStage: window.WindowStage): void {
    AppStorage.setOrCreate('context', this.context)
    AppStorage.setOrCreate('currentColorMode', this.context.config.colorMode);
    AppStorage.setOrCreate('windowStage', windowStage);
    AppStorage.setOrCreate<number>('statusBarHeight', CommonConstants.DEFAULT_STATUS_HEIGHT);
    WindowUtil.requestFullScreen(windowStage, this.context);
    windowStage.loadContent('pages/Index1', localStorage);
  }

  init_onWindowStageRestore(windowStage: window.WindowStage): void {
    AppStorage.setOrCreate<number>('statusBarHeight', CommonConstants.DEFAULT_STATUS_HEIGHT);
    WindowUtil.requestFullScreen(windowStage, this.context);
  }

  init_onForeground(): void {
    AppStorage.setOrCreate('currentColorMode', this.context.config.colorMode);
  }

  onContinue(wantParam: Record<string, Object>): AbilityConstant.OnContinueResult {
    wantParam["mainTabIndex"] = localStorage.get('mainTabIndex') as number;
    wantParam["mineTabIndex"] = localStorage.get('mineTabIndex') as number;
    // wantParam["userId"] = AppStorage.get<string>('userId') as string;
    // wantParam["articleItem"] = GlobalContext.getContext().getObject('articleItem') as string;
    // wantParam["stackName"] = GlobalContext.getContext().getObject('stackName') as string;
    // wantParam["userData"] = GlobalContext.getContext().getObject('userData') as string;
    // wantParam["learningItem"] = GlobalContext.getContext().getObject('learningItem') as string;
    // wantParam["introductionItem"] = GlobalContext.getContext().getObject('introductionItem') as string;
    wantParam["isLogged"] = AppStorage.get<boolean>('isLogged') as boolean;
    Logger.info(TAG, 'onContinue wantParam:' + JSON.stringify(wantParam));
    return AbilityConstant.OnContinueResult.AGREE;
  }

  async restoringData(want: Want, launchParam: AbilityConstant.LaunchParam): Promise<void> {
    Logger.info(TAG, 'Ability restoringData1');
    this.checkPermissions();
    // Recovering migrated data from want.
    if (launchParam.launchReason === AbilityConstant.LaunchReason.CONTINUATION) {
      Logger.info(TAG, 'Ability restoringData');
      if (want.parameters != undefined) {
        Logger.info(TAG, 'restoringData parameters:' + JSON.stringify(want.parameters));
        localStorage.setOrCreate('mainTabIndex', want.parameters.mainTabIndex)
        localStorage.setOrCreate('mineTabIndex', want.parameters.mineTabIndex)
        // GlobalContext.getContext().setObject('articleItem', want.parameters.articleItem);
        // GlobalContext.getContext().setObject('stackName', want.parameters.stackName);
        // GlobalContext.getContext().setObject('userData', want.parameters.userData);
        // GlobalContext.getContext().setObject('learningItem', want.parameters.learningItem);
        // GlobalContext.getContext().setObject('introductionItem', want.parameters.introductionItem);
        // GlobalContext.getContext().setObject('isContinue', true);
        // AppStorage.setOrCreate('userId', want.parameters.userId);
        AppStorage.setOrCreate('isLogged', want.parameters.isLogged);
      }
      this.context.restoreWindowStage(localStorage);
    }
  }

  async checkPermissions(): Promise<void> {
    const permissions: Array<Permissions> = ["ohos.permission.DISTRIBUTED_DATASYNC"];
    const accessManager = abilityAccessCtrl.createAtManager();
    try {
      const bundleFlags = bundleManager.BundleFlag.GET_BUNDLE_INFO_WITH_APPLICATION;
      const bundleInfo = await bundleManager.getBundleInfoForSelf(bundleFlags);
      const grantStatus = await accessManager.checkAccessToken(bundleInfo.appInfo.accessTokenId, permissions[0]);

      if (grantStatus === abilityAccessCtrl.GrantStatus.PERMISSION_DENIED) {
        accessManager.requestPermissionsFromUser(this.context, permissions)
      }
    } catch (err) {
      Logger.error('EntryAbility', 'checkPermissions', `Catch err: ${err}`);
      return;
    }
  }
}

```

```
import {
  CommonSizeConstants,
  Logger,
  NavDestinationPage,
  RouterManage,
  RouterMap,
  SizePreferencesUtil
} from '@geekapp/libgeekapp';
// import Theme, { ThemeStorage } from '@goweii/theme';

@Entry
@Component
struct Index1 {
  private pageInfos: NavPathStack = new NavPathStack();
  // @StorageLink('theme') theme: Theme = ThemeStorage.theme;

  onPageShow() {

  }

  aboutToAppear(): void {
    //
    this.pageInfos = RouterManage.instance.navPathStack;
    RouterManage.instance.replace({
      url: RouterMap.PageSplashNav,
      // url: RouterMap.PageNav,
      param: new Object({ query1: 1, query2: 2 }),

    });
    //
    // let param = this.routerController.getNavMapInfo(RouterMap.ResultPage)?.param as Object;
    // let params = JSONObject.parse(JSONObject.toJSONString(param));
    // // 识别码图返回结果
    // this.result = params.get('result') as Array<scanBarcode.ScanResult>;
    // this.uri = params.get('uri') as string;
    // this.infoW = params.get('infoW') as number;
  }

  @Builder
  PageMap(name: string) {
    NavDestinationPage({
      url: name,
      routerController: RouterManage._instance,
    })
  }

  build() {
    Navigation(this.pageInfos)
      .navDestination(this.PageMap)
      .hideNavBar(true)
      .hideTitleBar(true)
      .hideBackButton(true)
      .hideToolBar(true)
  }
}
```

## 约束与限制

在下述版本验证通过：
- DevEco Studio: NEXT Developer Preview2(4.1.3.700), SDK: API11(4.1.0(11))

## 目录结构

````
/libgeekapp    # 项目根目录
├── entry      # 示例代码文件夹
├── components     # components
│    └─ Logger
│     TextUtil                
├── README.md  # 安装使用方法                    
````

[//]: # ("pages/Index",)

[//]: # ("fontsize/pages/SetFontSizePage1",)

[//]: # ("webview/components/webview2/WebViewIndex2",)

[//]: # ("webview/components/webview3/WebViewIndex3",)

[//]: # ("webview/components/webview3/RouteToH5",)

[//]: # ("webview/components/webview3/Second",)

[//]: # ("webview/components/webview3/New",)

[//]: # ("scancode/pages/ScanBarcode",)

[//]: # ("scancode/pages/CustomScan",)

[//]: # ("scancode/pages/CreateBarcode",)

[//]: # ("scancode/pages/DetectBarcode",)

[//]: # ("scancode/pages/ResultPage",)

[//]: # ("scancode/pages/CustomResultPage")


## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/geekharmony/hmosapp1/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/geekharmony/hmosapp1/pulls) 。

## 开源协议

本项目基于 [Apache License](https://gitee.com/geekharmony/hmosapp1/blob/master/libgeekapp/LICENSE) ，请自由地享受和参与开源。

